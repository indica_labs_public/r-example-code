# Querying the GraphQL server with R

The script `r-api-example.R` is an example of how to send a GraphQL query to the Indica
Labs GraphQL API using the [R programming language](https://www.r-project.org/), signing
in using OIDC client credentials.
To use it, you will need to:

- install the `httpuv` and `httr` packages into your R environment for
  making HTTP requests and performing OAuth 2.0 authentication

- configure your Indica Labs identity provider to accept a
  client-credentials login from this program. A client ID, client
  secret, and the `graphql` and `serviceuser` scopes will need to be
  added to the identity provider configuration to tell the server it
  should allow access from an external program. For more details, see
  the identity provider documentation in
  C:/ProgramData/Indica Labs/Documentation/IndicaLabs.ApplicationLayer.Halo.IdentityProvider/setup.md.

- modify the script's `imagePK`, `query_gql` and `query_variables` variables to the desired GraphQL
  query to send to the server.

- you will need to know the appropriate URLs to the identity provider
  and GraphQL server for your organization's deployment.

- execute the script with `Rscript r-api-example.R`

The script operates as follows.

1. The user is prompted for the server URLs, client ID, and secret key.

1. A JSON payload is built for an example GraphQL query.

1. The `oauth2.0_token` routine sends the supplied client credentials
   to the Indica Labs identity provider to get an access token.

1. The `httr` library is used to send a `POST` request to the GraphQL
   server, containing a test GraphQL query in JSON format, and recieve
   the response back from the server.

1. The `data` variable contains the parsed query result, and is
   printed to the console.

For more information about the Indica Labs GraphQL API, see the
Markdown files in this folder:

  C:/ProgramData/Indica Labs/Documentation/IndicaLabs.ApplicationLayer.Halo.GraphQLApi/

For more information about configuring the Indica Labs identity
provider and how authentication works in the Indica Labs platform, see
the Markdown files in this folder:

  C:/ProgramData/Indica Labs/Documentation/IndicaLabs.ApplicationLayer.Halo.IdentityProvider/